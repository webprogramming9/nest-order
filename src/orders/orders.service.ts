import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from 'src/customers/entities/customer.entity';
import { Product } from 'src/products/entities/product.entity';
import { Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { OrderItem } from './entities/order-item';
import { Order } from './entities/order.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
    @InjectRepository(Customer)
    private customerRepository: Repository<Customer>,
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
  ) {}

  async create(createOrderDto: CreateOrderDto) {
    console.log(createOrderDto);
    const customer = await this.customerRepository.findOneBy({
      id: createOrderDto.customerId,
    });

    console.log(customer);
    const order: Order = new Order();
    order.customer = customer;
    order.amount = 0;
    order.total = 0;
    await this.ordersRepository.save(order);

    for (const od of createOrderDto.orderItems) {
      const orderItems = new OrderItem();
      orderItems.amount = od.amount;
      orderItems.product = await this.productsRepository.findOneBy({
        id: od.productId,
      });
      orderItems.name = orderItems.product.name;
      orderItems.price = orderItems.product.price;
      orderItems.total = orderItems.price * orderItems.amount;
      orderItems.order = order;
      this.orderItemsRepository.save(orderItems);
      order.amount = order.amount + orderItems.amount;
      order.total = order.total + orderItems.total;
    }
    await this.ordersRepository.save(order);
    return await this.ordersRepository.findOne({
      where: { id: order.id },
      relations: ['orderItems'],
    });
  }

  findAll() {
    return this.ordersRepository.find({
      relations: ['customers', 'orderitems'],
    });
  }

  findOne(id: number) {
    return this.ordersRepository.findOne({
      where: { id: id },
      relations: ['customers', 'orderitems'],
    });
  }

  update(id: number, updateOrderDto: UpdateOrderDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const order = await this.ordersRepository.findOneBy({ id: id });
    return this.orderItemsRepository.softRemove(order);
  }
}
